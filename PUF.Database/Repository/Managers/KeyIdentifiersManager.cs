﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PUF.Database.Entities;
using PUF.Database.Repository.Context;

namespace PUF.Database.Repository.Managers
{
    /// <summary>
    /// Определяет взаимодествие системы с ключами в базе данных
    /// </summary>
    public class KeyIdentifiersManager
    {
        /// <summary>
        /// Контекст базы данных
        /// </summary>
        private PUFContext _context;

        public KeyIdentifiersManager()
        {
            _context = new PUFContext();
        }

        public KeyIdentifiersManager(PUFContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Добавляет ключ в бд
        /// </summary>
        /// <param name="identifier"></param>
        public void Add(KeyIdentifier identifier)
        {
            _context.KeyIdentifiers.Add(identifier);
            _context.SaveChanges();
        }

        /// <summary>
        /// Изменяет ключ в бд
        /// </summary>
        /// <param name="identifier"></param>
        public void Change(KeyIdentifier identifier)
        {
            _context.KeyIdentifiers.Attach(identifier);
            _context.Entry(identifier).State = EntityState.Modified;
            _context.SaveChanges();
        }

        /// <summary>
        /// Удаляет ключ из бд
        /// </summary>
        /// <param name="identifier"></param>
        public void Delete(KeyIdentifier identifier)
        {
            _context.KeyIdentifiers.Remove(identifier);
            _context.SaveChanges();
        }

        /// <summary>
        /// Возвращает клю из бд
        /// </summary>
        /// <param name="id">Идентификатор ключа в бд</param>
        /// <returns></returns>
        public KeyIdentifier GetIdentifierById(int id)
        {
            KeyIdentifier identifier = null;

            try
            {
                identifier = _context.KeyIdentifiers.Where(x => x.KeyIdentifierId == id).First();
            }
            catch (Exception) { }

            return identifier;
        }

        /// <summary>
        /// Возвращает ключ из бд
        /// </summary>
        public List<KeyIdentifier> KeyIdentifiers
        {
            get
            {
                return _context.KeyIdentifiers.ToList();
            }
        }
    }
}
