﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

using PUF.Database.Repository.Context;
using PUF.Database.Entities;

namespace PUF.Database.Repository.Managers
{
    /// <summary>
    /// Определяет взаимодествие системы с запросами в базе данных
    /// </summary>
    public class KeyChallengesManager
    {
        /// <summary>
        /// Контекст базы данных
        /// </summary>
        private PUFContext _context;

        public KeyChallengesManager()
        {
            _context = new PUFContext();
        }

        public KeyChallengesManager(PUFContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Добавляет запрос в бд
        /// </summary>
        /// <param name="keyChallenge"></param>
        public void Add(KeyChallenge keyChallenge)
        {
            _context.KeyChallenges.Add(keyChallenge);
            _context.SaveChanges();
        }

        /// <summary>
        /// Изменяет запрос в бд
        /// </summary>
        /// <param name="keyChallenge"></param>
        public void Change(KeyChallenge keyChallenge)
        {
            _context.KeyChallenges.Attach(keyChallenge);
            _context.Entry(keyChallenge).State = EntityState.Modified;
            _context.SaveChanges();
        }

        /// <summary>
        /// Удаляет запрос из бд
        /// </summary>
        /// <param name="keyChallenge"></param>
        public void Delete(KeyChallenge keyChallenge)
        {
            _context.KeyChallenges.Remove(keyChallenge);
            _context.SaveChanges();
        }

        /// <summary>
        /// Извлекает из бд те запросы, которые относятся к определённому ключу
        /// </summary>
        /// <param name="keyIdentifier">Ключ, для которого необходимо извлечь запрос</param>
        /// <returns>Коллекция запросов для данного ключа</returns>
        public List<KeyChallenge> GetKeyChallengesByKeyIdentifier(KeyIdentifier keyIdentifier)
        {
            List<KeyChallenge> keyChallenges = _context.KeyChallenges.Where(x => x.KeyIdentifier == keyIdentifier).ToList();

            return KeyChallenges;
        }

        /// <summary>
        /// Возвращает все запросы системы из базы данных
        /// </summary>
        public List<KeyChallenge> KeyChallenges
        {
            get
            {
                return _context.KeyChallenges.ToList();
            }
        }
    }
}
