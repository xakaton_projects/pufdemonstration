﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PUF.Database.Entities;
using PUF.Database.Repository.Context;

namespace PUF.Database.Repository.Managers
{
    /// <summary>
    /// Объединяет менеджеры ключей и запросов в один контекст 
    /// для взаимодействия между ключами и запросами
    /// </summary>
    public class UnitOfWork
    {
        /// <summary>
        /// Контекст базы данных
        /// </summary>
        private PUFContext _context;

        /// <summary>
        /// Менеджер запросов
        /// </summary>
        private KeyChallengesManager _keyChallengesManager;

        /// <summary>
        /// Менеджер ключей
        /// </summary>
        private KeyIdentifiersManager _keyIdentifiersManager;
        
        public UnitOfWork()
        {
            _context = new PUFContext();
            _keyChallengesManager = new KeyChallengesManager(_context);
            _keyIdentifiersManager = new KeyIdentifiersManager(_context);
        }

        public KeyChallengesManager KeyChallengesManager
        {
            get
            {
                return _keyChallengesManager;
            }
        }

        public KeyIdentifiersManager KeyIdentifiersManager
        {
            get
            {
                return _keyIdentifiersManager;
            }
        }
    }
}
