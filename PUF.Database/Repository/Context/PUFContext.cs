﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

using PUF.Database.Entities;

namespace PUF.Database.Repository.Context
{
    public class PUFContext : DbContext
    {
        public PUFContext() : base("PUFDatabaseContext") { }

        public DbSet<KeyIdentifier> KeyIdentifiers { get; set; }

        public DbSet<KeyChallenge> KeyChallenges { get; set; }
    }
}
