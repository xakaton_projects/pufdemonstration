namespace PUF.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitializeDatabase : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.KeyChallenges",
                c => new
                    {
                        KeyChallengeId = c.Int(nullable: false, identity: true),
                        Challenge = c.String(),
                        Response = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.KeyChallengeId);
            
            CreateTable(
                "dbo.KeyIdentifiers",
                c => new
                    {
                        KeyIdentifierId = c.Int(nullable: false, identity: true),
                        KeyValue = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.KeyIdentifierId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.KeyIdentifiers");
            DropTable("dbo.KeyChallenges");
        }
    }
}
