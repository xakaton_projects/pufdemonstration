namespace PUF.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedmodelsKeyIdentifierandKeyChallenge : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.KeyChallenges", "KeyIdentifier_KeyIdentifierId", c => c.Int());
            CreateIndex("dbo.KeyChallenges", "KeyIdentifier_KeyIdentifierId");
            AddForeignKey("dbo.KeyChallenges", "KeyIdentifier_KeyIdentifierId", "dbo.KeyIdentifiers", "KeyIdentifierId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.KeyChallenges", "KeyIdentifier_KeyIdentifierId", "dbo.KeyIdentifiers");
            DropIndex("dbo.KeyChallenges", new[] { "KeyIdentifier_KeyIdentifierId" });
            DropColumn("dbo.KeyChallenges", "KeyIdentifier_KeyIdentifierId");
        }
    }
}
