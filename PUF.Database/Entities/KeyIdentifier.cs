﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace PUF.Database.Entities
{
    /// <summary>
    /// Модель ключа
    /// </summary>
    public class KeyIdentifier
    {
        /// <summary>
        /// Идентификатор ключа относительно базы данных
        /// </summary>
        public int KeyIdentifierId { get; set; }

        /// <summary>
        /// Идентификатор ключа
        /// </summary>
        public int KeyValue { get; set; }

        /// <summary>
        /// Коллекция запросов системы относительно данного ключа
        /// </summary>
        public List<KeyChallenge> KeyChallenges { get; set; }
    }
}