﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PUF.Database.Entities
{
    /// <summary>
    /// Модель запроса системы
    /// </summary>
    public class KeyChallenge
    {
        /// <summary>
        /// Id запроса системы относительно базы данных
        /// </summary>
        public int KeyChallengeId { get; set; }

        /// <summary>
        /// Ключ, к которому относится данных запрос
        /// </summary>
        public KeyIdentifier KeyIdentifier { get; set; }

        /// <summary>
        /// Запрос системы
        /// </summary>
        public string Challenge { get; set; }

        /// <summary>
        /// Ожидаемый ответ от ключа на данных запрос
        /// </summary>
        public bool Response { get; set; }
    }
}
