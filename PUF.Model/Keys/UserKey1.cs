﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PUF.Model.Keys
{
    /// <summary>
    /// Класс моделирующий ключ с физической ФНФ
    /// </summary>
    public class UserKey1 : KeyTemplate
    {
        
        public UserKey1(int id) : base(id) { }

        /// <summary>
        /// Метод имитирует ФНФ
        /// </summary>
        /// <param name="challenge">Запрос от системы для аутентификации</param>
        /// <returns>Ответ</returns>
        public override bool KeyResponse(string challenge)
        {
            bool[] array = ConvertFormStringToBoolArray(challenge);
            return (array[0] && !(array[array.Length - 1])) 
                || (!array[array.Length - 1] && array[0]);
        }

        /// <summary>
        /// Преобразование строки в массив элементов bool, так как в базе данных запрос хранится в виде строки
        /// </summary>
        /// <param name="stringToConvert">Запрос от системы</param>
        /// <returns>Запрос преобразованный в массив</returns>
        public bool[] ConvertFormStringToBoolArray(string stringToConvert)
        {
            bool[] newArray = new bool[stringToConvert.Length];

            for (int i = 0; i < newArray.Length; i++)
            {
                newArray[i] = stringToConvert[i] == '0' ? false : true;
            }

            return newArray;
        }
    }
}
