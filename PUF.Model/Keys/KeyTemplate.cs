﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PUF.Model.Keys
{
    /// <summary>
    /// Шаблон ключа для системы
    /// </summary>
    public abstract class KeyTemplate
    {
        /// <summary>
        /// Уникальный id ключа
        /// </summary>
        private readonly int _keyId;

        public KeyTemplate(int keyId)
        {
            _keyId = keyId;
        }

        /// <summary>
        /// Шаблон метода для реализации ФНФ
        /// </summary>
        /// <param name="challenge"></param>
        /// <returns></returns>
        public abstract bool KeyResponse(string challenge);

        /// <summary>
        /// Возвращает id ключа
        /// </summary>
        public int KeyId
        {
            get
            {
                return _keyId;
            }
        }
    }
}
